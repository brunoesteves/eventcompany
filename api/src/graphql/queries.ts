import { GraphQLList } from "graphql";

import { context } from "./context";

export const Query = {
  getAllRoles: async () => {
    try {
      let allRoles = await context.prisma.role.findMany();

      if (allRoles) {
        return allRoles;
      }
    } catch (error) {
      console.log(error);
    }
  },
  authenticationUser: async (_: any, args: Authentication) => {
    const { email, password } = args.input;
    console.log(args);
    try {
      let isUserAuthenticated = await context.prisma.user.findFirst({
        where: {
          email: email,
          password: password,
        },
      });
      if (isUserAuthenticated) return true;
    } catch (error) {
      throw new Error(
        "An internal error has occurred. Please try again later."
      );
    }
  },
};
