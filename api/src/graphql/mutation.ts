import { GraphQLString, GraphQLID } from "graphql";
import bcrypt from "bcrypt";

// import { RolesType, UserType, ProfessionalType } from "./typeDefs";
import { context } from "./context";

interface Values {
  id: number;
}
const saltRounds = 10;

const salt = bcrypt.genSaltSync(saltRounds);

export const Mutation = {
  createRole: async (_: any, args: { name: string }) => {
    const { name } = args;
    try {
      let newRole = await context.prisma.role.create({
        data: {
          name: name,
        },
      });

      if (newRole) return true;
    } catch (error) {
      throw new Error(
        "An internal error has occurred. Please try again later."
      );
    }
  },
  deleteRole: async (_: any, args: { id: number }) => {
    const { id } = args;

    try {
      let deleteRole = await context.prisma.role.delete({
        where: {
          id: Number(id),
        },
      });
      if (deleteRole) return true;
    } catch (error) {
      throw new Error(
        "An internal error has occurred. Please try again later."
      );
    }
  },
  addUser: async (_: any, args: newUser) => {
    console.log(args);

    const { name, email, password } = args.input;
    try {
      let newUser = await context.prisma.user.create({
        data: {
          name,
          email,
          password,
        },
      });
      if (newUser) return true;
    } catch (error) {
      throw new Error(
        "An internal error has occurred. Please try again later."
      );
    }
  },
};

// export const CREATE_ROLE: any = {
//   type: RolesType,
//   args: {
//     name: { type: GraphQLString },
//   },
//   async resolve(parent: any, args: newRole, context: Context) {
//     const { name } = args;

//     try {
//       let newRole = await context.prisma.role.create({
//         data: {
//           name: name,
//         },
//       });
//       if (newRole) {
//         return { status: true };
//       }
//     } catch (e) {
//       throw new Error("Ocorreu um erro interno. Tente novamente mais tarde.");
//     }
//   },
// };

// export const DELETE_ROLE: any = {
//   type: RolesType,
//   args: {
//     id: { type: GraphQLID },
//   },
//   async resolve(parent: any, args: deleteRole, context: Context) {
//     const { id } = args;
//     try {
//       let deleteRole = await context.prisma.role.delete({
//         where: {
//           id: Number(id),
//         },
//       });

//       return { status: deleteRole };
//     } catch (e) {
//       throw new Error("Ocorreu um erro interno. Tente novamente mais tarde.");
//     }
//   },
// };

// export const ADD_USER: any = {
//   type: UserType,
//   args: {
//     name: { type: GraphQLString },
//     email: { type: GraphQLString },
//     password: { type: GraphQLString },
//   },

//   async resolve(parent: any, args: newUser, context: Context) {
//     console.log("api: ", args);
//     const { name, email, password } = args;
//     const hashPassword = bcrypt.hashSync(password, salt);
//     try {
//       let newUser = await context.prisma.user.create({
//         data: {
//           name: name,
//           email: email,
//           password: hashPassword,
//         },
//       });

//       if (newUser) return { status: true };
//     } catch (error) {
//       throw new Error("Ocorreu um erro interno. Tente novamente mais tarde.");
//     }
//   },
// };

// export const AUTHENTICATION_USER: any = {
//   type: ProfessionalType,
//   args: {
//     email: { type: GraphQLString },
//     password: { type: GraphQLString },
//   },
//   async resolve(parent: any, args: Authentication, context: Context) {
//     const { email, password } = args;
//     try {
//       let authenticationProfessional = context.prisma.user.findFirst({
//         where: {
//           email: email,
//         },
//       });
//     } catch (error) {}
//   },
// };

// export const AUTHENTICATION_PROFESSIONAL: any = {
//   type: ProfessionalType,
//   args: {
//     email: { type: GraphQLString },
//     password: { type: GraphQLString },
//   },
//   async resolve(parent: any, args: Authentication, context: Context) {
//     const { email, password } = args;
//     try {
//       let authenticationProfessional = context.prisma.professional.findFirst({
//         where: {
//           email: email,
//         },
//       });
//     } catch (error) {}
//   },
// };

// export const ADD_PROFESSIONAL: any = {
//   type: ProfessionalType,
//   args: {
//     name: { type: GraphQLString },
//     email: { type: GraphQLString },
//     password: { type: GraphQLString },
//     address: { type: GraphQLString },
//     city: { type: GraphQLString },
//     state: { type: GraphQLString },
//     birthDate: { type: GraphQLString },
//     cpf: { type: GraphQLString },
//     phone: { type: GraphQLString },
//   },
//   async resolve(parent: any, args: Professional, context: Context) {
//     console.log(1111);
//     console.log(args);

//     const {
//       name,
//       email,
//       password,
//       address,
//       city,
//       state,
//       birthDate,
//       cpf,
//       phone,
//     } = args;

//     const hashPassword = bcrypt.hashSync(password, salt);

//     try {
//       console.log(222);

//       let addProfessional = await context.prisma.professional.create({
//         data: {
//           name: name,
//           email: email,
//           password: hashPassword,
//           address: address,
//           city: city,
//           state: state,
//           birthDate: birthDate,
//           cpf: cpf,
//           phone: phone,
//         },
//       });
//       if (addProfessional) return { status: true };
//     } catch (error) {
//       console.log(3333);
//     }
//   },
// };
