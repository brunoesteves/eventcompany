import { gql } from "apollo-server";

export const typeDefs = gql`
  type Roles {
    id: ID!
    name: String!
  }

  type Professionals {
    id: ID!
    name: String!
    email: String!
    password: String!
    address: String!
    city: String!
    state: String!
    birthDate: String!
    cpf: String!
    phone: String!
    status: Boolean!
  }

  type Users {
    id: ID!
    name: String!
    email: String!
    password: String!
    status: Boolean!
  }
  type Professional {
    name: String!
    email: String!
    password: String!
    address: String!
    city: String!
    state: String!
    birthDate: String!
    cpf: String!
    phone: String!
  }

  input AddProfessionalInput {
    name: String!
    email: String!
    password: String!
    address: String!
    city: String!
    state: String!
    birthDate: String!
    cpf: String!
    phone: String!
  }

  input AdduserInput {
    name: String!
    email: String!
    password: String!
  }

  input AuthenticationInput {
    email: String!
    password: String!
  }

  type Query {
    getAllRoles: [Roles!]
    authenticationProfessional(input: AuthenticationInput!): Boolean
    authenticationUser(input: AuthenticationInput!): Boolean
  }

  type Mutation {
    createRole(name: String!): Boolean!
    deleteRole(id: ID!): Boolean!
    addUser(input: AdduserInput): Boolean!
    addProfessional(input: AddProfessionalInput): Boolean
  }
`;
