// import express from "express";
// import cors from "cors";
// import { graphqlHTTP } from "express-graphql";
// // import { graphqlUploadExpress } from "graphql-upload";

// import { context } from "./graphql/context";
// import { schema } from "./graphql";

// const app = express();

// app.use(cors());

// app.use(express.json());

// app.use(
//   "/graphql",
//   //   graphqlUploadExpress({ maxFileSize: 1000000, maxFiles: 8 }),
//   graphqlHTTP({
//     schema,
//     context: context,
//     graphiql: true,
//   })
// );

// app.listen(3001, () => {
//   console.log("server running on 3001");
// });

import { ApolloServer } from "apollo-server";
import { Query } from "./graphql/queries";
import { Mutation } from "./graphql/mutation";

const resolvers = { Query, Mutation };

import { typeDefs } from "./graphql/typeDefs";
const server = new ApolloServer({ typeDefs, resolvers });

server.listen(process.env.PORT || 3001).then(() => {
  console.log("Yout API is Running at: 3001");
});
