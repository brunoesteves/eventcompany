interface Authentication {
  input: {
    email: string;
    password: string;
  };
}

interface Professional {
  name: string;
  email: string;
  password: string;
  address: string;
  city: string;
  state: string;
  birthDate: string;
  cpf: string;
  phone: string;
}

interface newUser {
  input: {
    name: string;
    email: string;
    password: string;
  };
}

interface newRole {
  name: string;
}

interface deleteRole {
  id: ID;
}
