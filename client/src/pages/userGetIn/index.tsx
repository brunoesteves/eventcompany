import React, { useEffect, useState } from "react";
import "./userGetIn.css";

import { BsEyeSlashFill, BsEye } from "react-icons/bs";
import { Field, Form, Formik } from "formik";
import { UserSignUp } from "libs/yup";
import { useLazyQuery } from "@apollo/client";
import { USER_AUTHENTICATION } from "libs/queries";
import { Link } from "react-router-dom";

export default function UserGetIn() {
  const [inputType, setInputType] = useState<boolean>(false);
  const [message, setMessage] = useState<string | null>("");
  const [userAuthentication, { data: isAdded }] =
    useLazyQuery(USER_AUTHENTICATION);

  const InitialValues: SignIn = {
    email: "",
    password: "",
  };

  useEffect(() => {
    setTimeout(() => {
      setMessage("");
    }, 3000);
  }, []);

  useEffect(() => {
    async function addUserStatus() {
      try {
        const res = await isAdded;

        if (res.addUser.status) {
          alert("User has been Added");
        }
      } catch (error) {}
    }
    addUserStatus();
  }, [isAdded]);

  return (
    <div className="body-add-user">
      <div>
        <img src={"/logotipo.png"} alt="logo" width={500} height={150} />
      </div>

      <div className="form">
        <div className="make_login">User Sign In </div>
        <Formik
          initialValues={InitialValues}
          validationSchema={UserSignUp}
          onSubmit={(values) => {
            const { email, password } = values;
            userAuthentication({
              variables: {
                input: { email, password },
              },
            });
            // actions.resetForm();
          }}
        >
          {({ errors, touched }) => (
            <Form>
              <div className="area">
                <Field
                  className="input_area"
                  placeholder="email"
                  name="email"
                />
                {errors.email && touched.email ? (
                  <div className="error">{errors.email}</div>
                ) : null}
              </div>

              <div className="area">
                <div className="password">
                  <Field
                    type={inputType ? "text" : "password"}
                    className="input_area"
                    placeholder="password"
                    name="password"
                  />

                  {!inputType ? (
                    <BsEye
                      onClick={() => setInputType(!inputType)}
                      className="eye"
                      color={"#fff"}
                    />
                  ) : (
                    <BsEyeSlashFill
                      onClick={() => setInputType(!inputType)}
                      className="eye"
                      color="#fff"
                    />
                  )}
                </div>

                <div className="base_input"></div>
                {errors.password && touched.password ? (
                  <div className="error">{errors.password}</div>
                ) : null}
                <div className="message">{message}</div>
              </div>

              <div className="area">
                <button className="signup_employee" type="submit">
                  Sign In
                </button>
              </div>
            </Form>
          )}
        </Formik>

        <Link to={"/users/signup"} className="create-account">
          ADD NEW USER
        </Link>
      </div>
    </div>
  );
}
