import React, { useEffect, useState } from "react";
import "./roles.css";
import { IoMdCloseCircleOutline } from "react-icons/io";
import { CREATE_ROLE, DELETE_ROLE } from "libs/mutations";
import { GET_ALL_ROLES } from "libs/queries";
import { useMutation, useQuery } from "@apollo/client";

export default function Roles() {
  const [roleName, setRoleName] = useState<string>("");
  const { data: roles, refetch } = useQuery<RolesData>(GET_ALL_ROLES);
  const [deleteRole, { data: isdeleted }] = useMutation(DELETE_ROLE);
  const [createRole, { data: isadded }] = useMutation(CREATE_ROLE);

  function deleteUniqueRole(index: number) {
    deleteRole({ variables: { id: index } });
  }

  async function newRole() {
    createRole({ variables: { name: roleName } });
  }

  useEffect(() => {
    refetch();
  }, [isadded, isdeleted, refetch]);

  return (
    <div className="container">
      <div>
        <input
          placeholder="add role"
          className="input_add_role"
          onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
            setRoleName(e.target.value)
          }
        />
        <div>
          <button className="send_role_button" onClick={() => newRole()}>
            Add
          </button>
        </div>
      </div>
      <div className="roles">
        {roles?.getAllRoles
          ?.slice(0)
          .sort()
          .map((role: RolesItem, i: number) => {
            return (
              <div className="role_option" key={i}>
                <div>{role.name}</div>
                <div className="deleteRole">
                  <IoMdCloseCircleOutline
                    onClick={() => deleteUniqueRole(role.id)}
                  />
                </div>
              </div>
            );
          })}
      </div>
    </div>
  );
}
