import React from "react";
import { Link } from "react-router-dom";
import "./home.css";

export default function Home() {
  return (
    <div className="container-home">
      <img src={"/logotipo.png"} alt="logo" width={500} height={150} />
      <div className="options-main">
        <div>
          {" "}
          <Link to={"/professional"} className="link-home">
            PROFESSIONAL
          </Link>
        </div>
        <div>
          {" "}
          <Link to={"/users"} className="link-home">
            USER
          </Link>
        </div>
      </div>
    </div>
  );
}
