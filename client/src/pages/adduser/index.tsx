import React, { useEffect, useState } from "react";
import "./addUser.css";

import { BsEyeSlashFill, BsEye } from "react-icons/bs";
import { Field, Form, Formik } from "formik";
import { UserSignUp } from "libs/yup";
import { useMutation } from "@apollo/client";
import { ADD_USER } from "libs/mutations";
import { Link } from "react-router-dom";

export default function AddUser() {
  const [inputType, setInputType] = useState<boolean>(false);
  const [message, setMessage] = useState<string | null>("");
  const [addUser, { data: isAdded }] = useMutation(ADD_USER);

  const InitialValues: InitialUserValues = {
    name: "",
    email: "",
    password: "",
  };

  useEffect(() => {
    setTimeout(() => {
      setMessage("");
    }, 3000);
  }, []);

  useEffect(() => {
    async function addUserStatus() {
      try {
        const res = await isAdded;

        if (res.addUser.status) {
          alert("User has been Added");
        }
      } catch (error) {}
    }
    addUserStatus();
  }, [isAdded]);

  return (
    <div className="body-add-user">
      <div>
        <img src={"/logotipo.png"} alt="logo" width={500} height={150} />
      </div>

      <div className="form">
        <div className="make_login">Sign Up User</div>
        <Formik
          initialValues={InitialValues}
          validationSchema={UserSignUp}
          onSubmit={(values) => {
            const { name, email, password } = values;
            addUser({
              variables: {
                input: { name, email, password },
              },
            });

            // actions.resetForm();
          }}
        >
          {({ errors, touched }) => (
            <Form>
              <div className="area">
                <Field
                  className="input_area"
                  placeholder="name"
                  type="text"
                  name="name"
                />
                {errors.name && touched.name ? (
                  <div className="error">{errors.name}</div>
                ) : null}
              </div>
              <div className="area">
                <Field
                  className="input_area"
                  placeholder="email"
                  name="email"
                />
                {errors.email && touched.email ? (
                  <div className="error">{errors.email}</div>
                ) : null}
              </div>

              <div className="area">
                <div className="password">
                  <Field
                    type={inputType ? "text" : "password"}
                    className="input_area"
                    placeholder="password"
                    name="password"
                  />

                  {!inputType ? (
                    <BsEye
                      onClick={() => setInputType(!inputType)}
                      className="eye"
                      color={"#fff"}
                    />
                  ) : (
                    <BsEyeSlashFill
                      onClick={() => setInputType(!inputType)}
                      className="eye"
                      color="#fff"
                    />
                  )}
                </div>

                <div className="base_input"></div>
                {errors.password && touched.password ? (
                  <div className="error">{errors.password}</div>
                ) : null}
                <div className="message">{message}</div>
              </div>
              <div className="area">
                <div className="password">
                  <Field
                    type={inputType ? "text" : "password"}
                    className="input_area"
                    placeholder="confirm password"
                    name="confirmPassword"
                  />

                  {!inputType ? (
                    <BsEye
                      onClick={() => setInputType(!inputType)}
                      className="eye"
                      color={"#fff"}
                    />
                  ) : (
                    <BsEyeSlashFill
                      onClick={() => setInputType(!inputType)}
                      className="eye"
                      color={"#fff"}
                    />
                  )}
                </div>

                <div className="base_input"></div>
                {errors.password && touched.password ? (
                  <div className="error">{errors.password}</div>
                ) : null}
                <div className="message">{message}</div>
              </div>
              <div className="area">
                <button className="signup_employee" type="submit">
                  Sign Up
                </button>
              </div>
            </Form>
          )}
        </Formik>

        <Link to={"/users"} className="create-account">
          USER SIGN IN
        </Link>
      </div>
    </div>
  );
}
