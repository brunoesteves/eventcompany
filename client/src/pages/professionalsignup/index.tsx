import React, { useEffect, useState } from "react";
import "./professionalSignUp.css";

import { BsEyeSlashFill, BsEye } from "react-icons/bs";
import { Field, Form, Formik } from "formik";
import { ProfessionalSchema } from "libs/yup";
import { useMutation } from "@apollo/client";
import { ADD_PROFESSIONAL } from "libs/mutations";
import { Link } from "react-router-dom";

import InputMask from "react-input-mask";
import { cities, states } from "libs/citiesStattes";

export default function ProfessionalSignUp() {
  const [inputType, setInputType] = useState<boolean>(false);
  const [message, setMessage] = useState<string | null>("");
  const [addProfessional, { data: isAdded }] = useMutation(ADD_PROFESSIONAL);

  const InitialValues: InitialProfessionalValuesSignUp = {
    email: "",
    password: "",
    name: "",
    address: "",
    city: "",
    state: "",
    birthDate: "",
    cpf: "",
    phone: "",
  };
  useEffect(() => {
    setTimeout(() => {
      setMessage("");
    }, 3000);
  }, []);

  useEffect(() => {
    async function addProfessionalStatus() {
      try {
        const res = await isAdded;

        if (res.authenticated.status) {
          alert("Professional has been Added");
        }
      } catch (error) {}
    }
    addProfessionalStatus();
  }, [isAdded]);

  return (
    <div className="body-add-user">
      <div>
        <img src={"/logotipo.png"} alt="logo" width={500} height={150} />
      </div>

      <div className="form">
        <div className="make_login">Sign In Professional</div>
        <Formik
          initialValues={InitialValues}
          validationSchema={ProfessionalSchema}
          onSubmit={(values) => {
            addProfessional({
              variables: {
                name: values.name,
                email: values.email,
                password: values.password,
                address: values.address,
                city: values.city,
                state: values.state,
                birthDate: values.birthDate,
                cpf: values.cpf,
                phone: values.phone,
              },
            });

            // actions.resetForm();
          }}
        >
          {({ errors, touched }) => (
            <Form style={{ backgroundClip: "red", width: "40vw" }}>
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-evenly",
                }}
              >
                <div style={{ width: "45%" }}>
                  <div className="area">
                    <Field
                      className="input_area"
                      placeholder="name"
                      type="text"
                      name="name"
                    />
                    {errors.name && touched.name ? (
                      <div className="error">{errors.name}</div>
                    ) : null}
                  </div>
                  <div className="area">
                    <Field
                      className="input_area"
                      placeholder="email"
                      type="text"
                      name="email"
                    />
                    {errors.email && touched.email ? (
                      <div className="error">{errors.email}</div>
                    ) : null}
                  </div>
                  <div className="area">
                    <Field
                      className="input_area"
                      placeholder="address"
                      type="text"
                      name="address"
                    />
                    {errors.address && touched.address ? (
                      <div className="error">{errors.address}</div>
                    ) : null}
                  </div>
                  <div className="area">
                    <Field as="select" className="select_area" name="city">
                      <option>select a city</option>
                      {cities.map((city, i) => {
                        return (
                          <option key={i} value={city.label}>
                            {city.label}
                          </option>
                        );
                      })}
                    </Field>
                    {errors.city && touched.city ? (
                      <div className="error">{errors.city}</div>
                    ) : null}
                  </div>
                  <div className="area">
                    <Field
                      as="select"
                      className="select_area"
                      placeholder="estado"
                      type="text"
                      name="state"
                      autocomplet="on"
                    >
                      <option>select a state</option>
                      {states.map((state, i) => {
                        return (
                          <option key={i} value={state.label}>
                            {state.label}
                          </option>
                        );
                      })}
                    </Field>
                    {errors.state && touched.state ? (
                      <div className="error">{errors.state}</div>
                    ) : null}
                  </div>
                </div>
                <div style={{ width: "45%" }}>
                  <div className="area">
                    <Field
                      className="input_area"
                      placeholder="phone"
                      type="text"
                      name="phone"
                      as={InputMask}
                      mask="(99)99999-9999"
                    />
                    {errors.phone && touched.phone ? (
                      <div className="error">{errors.phone}</div>
                    ) : null}
                  </div>
                  <div className="area">
                    <Field
                      className="date_area"
                      placeholder="birthDate"
                      type="date"
                      name="birthDate"
                    />
                    {errors.birthDate && touched.birthDate ? (
                      <div className="error">{errors.birthDate}</div>
                    ) : null}
                  </div>
                  <div className="area">
                    <Field
                      className="input_area"
                      placeholder="cpf"
                      type="text"
                      name="cpf"
                      as={InputMask}
                      mask="9999.9999-99"
                    />
                    {errors.cpf && touched.cpf ? (
                      <div className="error">{errors.cpf}</div>
                    ) : null}
                  </div>

                  <div className="area">
                    <div className="password">
                      <Field
                        type={inputType ? "text" : "password"}
                        className="input_area"
                        placeholder="password"
                        name="password"
                      />

                      {!inputType ? (
                        <BsEye
                          onClick={() => setInputType(!inputType)}
                          className="eye"
                          color={"#fff"}
                        />
                      ) : (
                        <BsEyeSlashFill
                          onClick={() => setInputType(!inputType)}
                          className="eye"
                          color={"#fff"}
                        />
                      )}
                    </div>

                    <div className="base_input"></div>
                    {errors.password && touched.password ? (
                      <div className="error">{errors.password}</div>
                    ) : null}
                    <div className="message">{message}</div>
                  </div>
                  <div className="area">
                    <div className="password">
                      <Field
                        type={inputType ? "text" : "password"}
                        className="input_area"
                        placeholder="confirm password"
                        name="confirmPassword"
                      />

                      {!inputType ? (
                        <BsEye
                          onClick={() => setInputType(!inputType)}
                          className="eye"
                          color={"#fff"}
                        />
                      ) : (
                        <BsEyeSlashFill
                          onClick={() => setInputType(!inputType)}
                          className="eye"
                          color={"#fff"}
                        />
                      )}
                    </div>

                    <div className="base_input"></div>
                    {errors.password && touched.password ? (
                      <div className="error">{errors.password}</div>
                    ) : null}
                    <div className="message">{message}</div>
                  </div>
                </div>
              </div>
              <button className="signup" type="submit">
                Sign up
              </button>
            </Form>
          )}
        </Formik>

        <div className="create-account">Have an account?</div>

        <Link to={"/professional"} className="create-account">
          Click here
        </Link>
      </div>
    </div>
  );
}
