import { gql } from "@apollo/client";

export const DELETE_ROLE = gql`
  mutation DeleteRole($id: ID!) {
    deleteRole(id: $id)
  }
`;

export const CREATE_ROLE = gql`
  mutation CreateRole($name: String!) {
    createRole(name: $name)
  }
`;

export const ADD_USER = gql`
  mutation AddUser($input: AdduserInput) {
    addUser(input: $input)
  }
`;

export const ADD_PROFESSIONAL = gql`
  mutation AddProfessional($input: AddProfessionalInput!) {
    addProfessional(input: $input)
  }
`;

// 2Wwwwwwwwwe@
