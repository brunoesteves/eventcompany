import * as Yup from "yup";

export const ProfessionalSchema = Yup.object().shape({
  name: Yup.string().required("Required"),
  email: Yup.string().email("Email Inválido").required("Required"),
  address: Yup.string().required("Required"),
  city: Yup.string().required("Required"),
  state: Yup.string().required("Required"),
  cpf: Yup.string().required("Required"),
  phone: Yup.string().required("Required"),
  birthDate: Yup.string().required("Required"),
  password: Yup.string()
    .required("Digite uma senha")
    .matches(
      /^.*(?=.{8,})((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/,
      "A senha deve conter pelo menos 8 caracteres, uma maiúscula, um número e um caractere especial"
    ),
  confirmPassword: Yup.string()
    .required("Por favor Confirme a senha")
    .when("password", {
      is: (password: string) =>
        password && password.length > 0 ? true : false,
      then: Yup.string().oneOf([Yup.ref("password")], "Senhas diferentes"),
    }),
});

export const UserSignUp = Yup.object().shape({
  name: Yup.string().required("Required"),
  email: Yup.string().email("Email Inválido").required("Required"),
  password: Yup.string()
    .required("Digite uma senha")
    .matches(
      /^.*(?=.{8,})((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/,
      "A senha deve conter pelo menos 8 caracteres, uma maiúscula, um número e um caractere especial"
    ),
  confirmPassword: Yup.string()
    .required("Por favor Confirme a senha")
    .when("password", {
      is: (password: string) =>
        password && password.length > 0 ? true : false,
      then: Yup.string().oneOf([Yup.ref("password")], "Senhas diferentes"),
    }),
});

export const LoginSchema = Yup.object().shape({
  userEmail: Yup.string().email("Email Inválido").required("Required"),
  password: Yup.string().required("Digite a senha"),
});

export const EventSchema = Yup.object().shape({
  title: Yup.string().required(""),
  allDay: Yup.string().required("Required"),
  start: Yup.string().required("Required"),
  end: Yup.string().required("Required"),
  subject: Yup.string().required("Required"),
});
