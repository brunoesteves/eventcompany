import React, { useEffect, useRef, useState } from "react";
import styles from "../styles/selectDropDown.module.css";

interface Option {
  label: string;
}

interface ListOptions {
  options: Option[];
  placeholder: string;
}

export default function SelectDropDown({ options, placeholder }: ListOptions) {
  const [allCountries] = useState<string[]>([]);
  const [filterCountries, seFilterCountries] = useState<string[]>(allCountries);
  const [inputTerm, setInputTerm] = useState<string>();
  const refOne = useRef<any>(null);
  const [openList, setOpenList] = useState<boolean>(true);

  function filterData(data: string[], searchTerm: string) {
    return data.filter((x: string) =>
      x.toLowerCase().includes(searchTerm.toLowerCase())
    );
  }

  const items = (countries: string[]) => {
    return countries.map((country: string, i: number) => {
      return (
        <li
          key={i}
          onClick={() => {
            const filteredCountries = filterData(allCountries, country);
            seFilterCountries(filteredCountries);
            setInputTerm(country);
          }}
        >
          {country}
        </li>
      );
    });
  };
  useEffect(() => {
    document.addEventListener("click", handleClickOutside, true);
    options.forEach((option: any) => {
      allCountries.push(option.label);
    });
  }, []);

  const handleClickOutside = (e: { target: any }) => {
    if (refOne.current.contains(e.target)) {
      console.log("sim");
      setOpenList(true);
    } else {
      setOpenList(false);
      console.log("nao");
    }
  };

  return (
    <div className={styles.container}>
      <input
        ref={refOne}
        placeholder={placeholder}
        className={styles.autocomplete_input}
        value={inputTerm}
        onChange={(e) => {
          setInputTerm(e.target.value);
          const filteredCountries = filterData(allCountries, e.target.value);
          seFilterCountries(filteredCountries);
        }}
      ></input>
      <ul
        className={
          openList ? styles.autocomplete_list : styles.autocomplete_list_out
        }
      >
        {items(filterCountries)}
      </ul>
    </div>
  );
}
