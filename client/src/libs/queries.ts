import { gql } from "@apollo/client";

export const GET_ALL_ROLES = gql`
  query GetAllRoles {
    getAllRoles {
      name
      id
    }
  }
`;

export const PROFESSIONAL_AUTHENTICATION = gql`
  query AuthenticationProfessional($input: AuthenticationInput!) {
    authenticationProfessional(input: $input)
  }
`;

export const USER_AUTHENTICATION = gql`
  query AuthenticationUser($input: AuthenticationInput!) {
    authenticationUser(input: $input)
  }
`;
