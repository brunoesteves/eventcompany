import React from "react";
import "./header.css";
import { AiOutlinePoweroff } from "react-icons/ai";
import { Link } from "react-router-dom";

export default function Header() {
  return (
    <div className="body-header">
      <div>
        <Link to={"/"}>
          <img src={"/logotipo.png"} alt="logotype" width={150} height={50} />
        </Link>
      </div>
      <div className="right">
        <img src={"/avatar.png"} alt="logo" className="picture-profile" />
        <AiOutlinePoweroff />
      </div>
    </div>
  );
}
