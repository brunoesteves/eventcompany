import React from "react";
import "./CommonProfessional.css";

import Header from "../../header";
import { Outlet } from "react-router-dom";

export default function CommonProfessional() {
  return (
    <div>
      <Header />
      <div className="main-professional">
        <Outlet />
      </div>
    </div>
  );
}
