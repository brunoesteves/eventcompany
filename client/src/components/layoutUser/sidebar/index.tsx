import React from "react";
import "./sidebar.css";
import { Link } from "react-router-dom";

export default function Sidebar() {
  return (
    <ul>
      <Link to={"/"} className="link">
        Home
      </Link>
      <Link to={"/search"} className="link">
        Search Professional
      </Link>
      <Link to={"/events"} className="link">
        Manage Events
      </Link>
      <Link to={"/roles"} className="link">
        Manage Roles
      </Link>
    </ul>
  );
}
