import React from "react";
import "./commonUser.css";
import { Outlet } from "react-router-dom";

import Sidebar from "../sidebar";
import Header from "../../header";

export default function CommonUser() {
  return (
    <div>
      <div className="header">
        <Header />
      </div>
      <div>
        <div className="sidebar">
          <Sidebar />
        </div>
        <div className="main-user">
          <Outlet />
        </div>
      </div>
    </div>
  );
}
