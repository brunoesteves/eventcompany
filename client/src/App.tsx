import * as React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";

import Home from "pages/home";
import CommonUser from "components/layoutUser/common";
import AddUser from "./pages/adduser";
import UserGetIn from "./pages/userGetIn";
import Roles from "pages/roles";
import CommonProfessional from "components/layoutProfessional/common";
import ProfessionalSignIn from "pages/professionalSignIn";
import ProfessionalSignUp from "pages/professionalsignup";

export default function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/users" element={<CommonUser />}>
          <Route path="" element={<UserGetIn />} />
          <Route path="signup" element={<AddUser />} />
          <Route path="roles" element={<Roles />} />
        </Route>
        <Route path="/professional" element={<CommonProfessional />}>
          <Route path="" element={<ProfessionalSignIn />} />
          <Route path="professionalsignup" element={<ProfessionalSignUp />} />
        </Route>
        {/* <Route path="*" element={<Error />} />  */}
      </Routes>
    </BrowserRouter>
  );
}
