interface InitialUserValues {
  name: string;
  email: string;
  password: string;
}

interface InitialProfessionalValuesSignUp {
  name: string;
  email: string;
  password: string;
  address: string;
  city: string;
  state: string;
  birthDate: string;
  cpf: string;
  phone: string;
}

interface SignIn {
  email: string;
  password: string;
}

interface RolesItem {
  name: string;
  id: number;
}

interface RolesData {
  getAllRoles: RolesItem[];
}
